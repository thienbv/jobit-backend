const bcrypt = require("bcrypt");
const saltRounds = 10;
const salt = () => {
    return bcrypt.genSaltSync(saltRounds)
}
const hashPassword = password => {
  return bcrypt.hashSync(password, salt());
};
const checkPassword = async (password, passwordHash) => {
  return await bcrypt.compareSync(password, passwordHash);
};
module.exports = {
  hashPassword: hashPassword,
  checkPassword: checkPassword,
};
