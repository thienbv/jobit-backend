'use strict';

const express = require('express');
const validate = require('express-validation');
const val = require('../validations/user');
const applyValidate = require('../validations/apply');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const loggerCommon = require('../utils/logger');
const {checkPassword} = require('../utils/hash_password');
const requireAuthUser = require('../middlewares/requireAuthUser');
const { CloudinaryStorage } = require('multer-storage-cloudinary');

const CONSTANTS = require('../constants/Globals');

const dotenv = require('dotenv');
dotenv.config();
const SECRET_KEY = process.env.SECRET_KEY;

const {cloudinary} = require('../cloudinary');

//Upload avatar
const fs = require('fs');
const multer = require('multer');
const maxSize = 2 * 1024 * 1024; //2mb
/*
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, __dirname + '/../uploads/users/avatar');
  },
  filename: function(req, file, cb) {
    let imageName = file.originalname.split('.')[0].replace(/ /gi, '-');
    let typeImage = file.mimetype.split('/')[1];
    cb(null, `${imageName}${Date.now()}.${typeImage}`);
  },
});
*/
const storage = new CloudinaryStorage({
  cloudinary: cloudinary,
  params: {
    folder: 'avatars',
    // format: async (req, file) => 'png', // supports promises as well
    public_id: (req, file) => {
      let imageName = file.originalname.split('.')[0].replace(/ /gi, '-');
      let typeImage = file.mimetype.split('/')[1];
      return `${imageName}${Date.now()}.${typeImage}`
    },
  },
});

const fileFilter = (req, file, cb) => {
  let mimetypes = [
    'image/png',
    'image/jpg',
    'image/jpeg',
    'image/bmp',
    'image/tif',
    'image/tiff',
    'image/thm',
  ];
  if (!mimetypes.includes(file.mimetype)) {
    req.fileValidationError = 'Wrong format image';
    return cb(null, false, new Error('avatar invalid'));
  }
  cb(null, true);
};

const limits = {fileSize: maxSize};

const upload = multer({storage, fileFilter, limits});
//Apply methods
const {applyJob, isApplyJob} = require('../models/applyModel');
//User methods
const {
  createUser,
  updateUser,
  findUserByOption,
  findAllUser,
  isExistUser,
} = require('../models/userModel');

//Report methods
const {
  createReport,
  updateReport,
  findReportById,
  findReportByUserId,
} = require('../models/reportModel');

//Vote methods
const {
  createVote,
  updateVote,
  findVoteByUserId,
  findVoteById,
} = require('../models/voteModel');
const logger = loggerCommon.getLogger('user-api');
const router = express.Router();


router.post('/create', validate(val.create), async (req, res) => {
  const userDto = req.body;
  //Validate email
  const isExits = await isExistUser(userDto.email);
  if (isExits) {
    return res.status(400).json({
      success: false,
      messages: `Email ${userDto.email} existed!`,
    });
  }

  logger.info('================= Create User =================');
  try {
    //save userOnline to DB Offline
    delete userDto.confirmPassword;
    logger.info(JSON.stringify(userDto));
    const resultCreate = await createUser(userDto);
    return res.send({
      success: true,
      user_id: resultCreate.insertId,
    });
  } catch (err) {
    logger.error(err);
    res.status(400).json({
      success: false,
      messages: `Create User fail ${err}`,
    });
  }
});

router.post('/login', async (req, res) => {
  const {email, password} = req.body;
  if (!email || !password) {
    return res.status(422).send({
      error: 'Must provide Email and Password!',
    });
  }
  let user = await findUserByOption(email, CONSTANTS.TYPE_FIND.EMAIL);
  if (!user) {
    return res.status(422).send({
      error: 'Invalid Email or Password',
    });
  }
  try {
    const checkPass = await checkPassword(password, user.password);
    if (checkPass) {
      //Get Info User Online
      const expired = '48h'; //1hour
      const dataSign = {
        user_id: user.user_id,
      };
      const token = jwt.sign(dataSign, SECRET_KEY, {
        expiresIn: expired,
      });
      delete user.password;
      return res.send({
        token,
        user,
      });
    } else {
      return res.status(422).send({
        error: 'Invalid Password or UserName',
      });
    }
  } catch (e) {
    return res.status(422).send({
      error: 'Invalid Password or UserName',
    });
  }
});

//Get All User
router.get('/getAllUsers', async (req, res) => {
  try {
    const users = await findAllUser();
    res.json({success: true, users});
  } catch (e) {
    logger.error(e);
    res.status(404).json({message: 'Something went wrong!'});
  }
});

//Update user info
router.post(
  '/update',
  [requireAuthUser, upload.single('avatar')],
  async (req, res) => {
    try {
      let {user} = req;
      let userDto = req.body;
      if (req.file) {
        userDto.avatar = req.file.path;
      }
      let arrFiledNotUpdate = ['user_id', 'email', 'role', 'type'];
      let fieldsUpdate = Object.keys(userDto);
      for (let field of fieldsUpdate) {
        if (!arrFiledNotUpdate.includes(field)) {
          if (field == 'birth_day') {
            user[field] = moment(userDto[field]).format('YYYY-MM-DD');
          }
          user[field] = userDto[field];
        }
      }

      const result = await updateUser(user);
      if (result.affectedRows > 0) {
        const userUpdate = await findUserByOption(
          user.user_id,
          CONSTANTS.TYPE_FIND.USER_ID
        );
        delete userUpdate.password;
        userUpdate.birth_day = moment(userUpdate.birth_day).format(
          'DD/MM/YYYY'
        );
        userUpdate.created_at = moment(userUpdate.created_at).format(
          'DD/MM/YYYY HH:mm'
        );
        userUpdate.updated_at = moment(userUpdate.updated_at).format(
          'DD/MM/YYYY HH:mm'
        );
        res.json({success: true, user: userUpdate});
      } else {
        res.status(400).json({message: 'Something went wrong!'});
      }
    } catch (error) {
      logger.error(error);
      res.status(400).json({message: 'Something went wrong!'});
    }
  }
);

//User report company
router.post('/report', requireAuthUser, async (req, res) => {
  try {
    const {user} = req;
    if (user.role != CONSTANTS.ROLES.ADMIN) {
      if (user.type != CONSTANTS.TYPE_USER.CANDIDATE) {
        return res.status(404).json({message: `You are not candidate!`});
      }
    }
    const reportDto = req.body;
    reportDto.user_id = user.user_id;
    const result = await createReport(reportDto);
    if (result.affectedRows > 0) {
      const report = await findReportById(result.insertId);
      return res.json({success: true, report});
    } else {
      return res.status(404).json({message: 'Something went wrong!'});
    }
  } catch (error) {
    logger.error(error);
    res.status(404).json({message: 'Something went wrong!'});
  }
});

//User update report company
router.put('/report/update/:report_id', requireAuthUser, async (req, res) => {
  try {
    const {user} = req;
    if (user.role != CONSTANTS.ROLES.ADMIN) {
      if (user.type != CONSTANTS.TYPE_USER.CANDIDATE) {
        return res.status(404).json({message: `You are not candidate!`});
      }
    }
    const {report_id} = req.params;
    const report = await findReportById(report_id);
    if (!report) {
      return res.status(404).json({message: `Not found report!`});
    }
    const reportDto = req.body;

    reportDto.user_id = user.user_id;
    report.comment = reportDto.comment;

    const resultReport = await updateReport(report);

    if (resultReport.affectedRows > 0) {
      const report = await findReportById(report_id);
      return res.json({success: true, report});
    } else {
      res.status(404).json({message: 'Something went wrong!'});
    }
  } catch (error) {
    logger.error(error);
    res.status(404).json({message: 'Something went wrong!'});
  }
});
//User apply job
router.post(
  '/apply',
  [requireAuthUser, validate(applyValidate.create)],
  async (req, res) => {
    try {
      const {user} = req;
      if (user.type != CONSTANTS.TYPE_USER.CANDIDATE) {
        return res.status(404).json({message: `Only candidate can apply!`});
      }
      const {job_id} = req.body;
      const isApplied = await isApplyJob(user.user_id, job_id);
      if (isApplied) {
        return res.status(404).json({message: `You had applied!`});
      }
      await applyJob(user.user_id, job_id);
      res.json({success: true});
    } catch (error) {
      logger.error(error);
      res.status(404).json({message: 'Something went wrong!'});
    }
  }
);

//User vote company
router.post(
  '/vote',
  [requireAuthUser, validate(val.create_vote)],
  async (req, res) => {
    try {
      const {user} = req;
      if (user.role != CONSTANTS.ROLES.ADMIN) {
        if (user.type != CONSTANTS.TYPE_USER.CANDIDATE) {
          return res.status(404).json({message: `You are not candidate!`});
        }
      }
      const voteDto = req.body;
      voteDto.user_id = user.user_id;
      const voteCreated = await createVote(voteDto);
      const voteAfterCreated = await findVoteById(voteCreated.insertId);
      const {user_id, user_id_voted} = voteAfterCreated;
      const userVote = await findUserByOption(
        user_id,
        CONSTANTS.TYPE_FIND.USER_ID
      );
      const userVoted = await findUserByOption(
        user_id_voted,
        CONSTANTS.TYPE_FIND.USER_ID
      );

      delete userVote.password;
      delete userVoted.password;

      voteAfterCreated.userVote = userVote;
      voteAfterCreated.userVoted = userVoted;
      return res.json({success: true, vote: voteAfterCreated});
    } catch (error) {
      logger.error(error);
      res.status(404).json({message: 'Something went wrong!'});
    }
  }
);

//User vote company
router.put(
  '/vote/update/:vote_id',
  [requireAuthUser, validate(val.update_vote)],
  async (req, res) => {
    try {
      const {user} = req;
      if (user.role != CONSTANTS.ROLES.ADMIN) {
        if (user.type != CONSTANTS.TYPE_USER.CANDIDATE) {
          return res.status(404).json({message: `You are not candidate!`});
        }
      }
      const {vote_id} = req.params;
      const vote = await findVoteById(vote_id);
      if (!vote) {
        return res.status(404).json({message: `Not found vote!`});
      }
      const voteDto = req.body;

      voteDto.user_id = user.user_id;
      vote.comment = voteDto.comment;
      vote.vote = voteDto.vote;

      const resultVote = await updateVote(vote);
      if (resultVote.affectedRows > 0) {
        const voteAfterUpdate = await findVoteById(vote.vote_id);
        const {user_id, user_id_voted} = voteAfterUpdate;
        const userVote = await findUserByOption(
          user_id,
          CONSTANTS.TYPE_FIND.USER_ID
        );
        const userVoted = await findUserByOption(
          user_id_voted,
          CONSTANTS.TYPE_FIND.USER_ID
        );

        delete userVote.password;
        delete userVoted.password;

        voteAfterUpdate.userVote = userVote;
        voteAfterUpdate.userVoted = userVoted;
        return res.json({success: true, vote: voteAfterUpdate});
      } else {
        res.status(404).json({message: 'Something went wrong!'});
      }
    } catch (error) {
      logger.error(error);
      res.status(404).json({message: 'Something went wrong!'});
    }
  }
);

module.exports = router;
