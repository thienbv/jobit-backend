'use strict';

const express = require('express');
const validate = require('express-validation');
const val = require('../validations/job');
const moment = require('moment');
const loggerCommon = require('../utils/logger');
const requireAuthUser = require('../middlewares/requireAuthUser');

const CONSTANTS = require('../constants/Globals');

const dotenv = require('dotenv');
dotenv.config();

const {
  createJob,
  findAll,
  findJobByUserId,
  findJobById,
  updateJob,
  searchJobByOption,
  getDetail,
  getUsersApply,
} = require('../models/jobModel');

const logger = loggerCommon.getLogger('job-api');
const router = express.Router();

router.post(
  '/create',
  [requireAuthUser, validate(val.create)],
  async (req, res) => {
    const {user} = req;

    if (
      user.role != CONSTANTS.ROLES.ADMIN &&
      user.type != CONSTANTS.TYPE_USER.COMPANY
    ) {
      return res.status(400).json({message: `You don't have permission!`});
    }

    const jobDto = req.body;
    jobDto.user_id = user.user_id;
    jobDto.positions = jobDto.positions.toLowerCase();
    jobDto.languages = jobDto.languages.toLowerCase();

    logger.info('================= Create JOB =================');
    try {
      logger.info(JSON.stringify(jobDto));
      const resultCreate = await createJob(jobDto);
      return res.send({
        success: true,
        job_id: resultCreate.insertId,
      });
    } catch (err) {
      logger.error(err);
      res.status(400).json({
        success: false,
        messages: `Create Job fail ${err}`,
      });
    }
  }
);

//Get All User
router.get('/getAll', async (req, res) => {
  try {
    const jobs = await findAll();
    res.json({success: true, jobs});
  } catch (e) {
    logger.error(e);
    res.status(404).json({message: 'Something went wrong!'});
  }
});

//Get Job By UserID
router.get('/getByUser', requireAuthUser, async (req, res) => {
  try {
    const {user} = req;
    const jobs = await findJobByUserId(user.user_id);
    res.json({success: true, jobs});
  } catch (error) {
    logger.error(error);
    res.status(400).json({message: 'Something went wrong!'});
  }
});
//Get jobs by option
router.get('/search', async (req, res) => {
  try {
    const {option, query, salary_min, salary_max, salary_type} = req.query;
    const lstOption = [
      CONSTANTS.OPTION_SEARCH_JOB.COMPANY,
      CONSTANTS.OPTION_SEARCH_JOB.LOCATION,
      CONSTANTS.OPTION_SEARCH_JOB.POSITION,
    ];
    const lstSalaryType = [
      CONSTANTS.SALARY_TYPE.VND,
      CONSTANTS.SALARY_TYPE.USD,
    ];
    if (!lstOption.includes(option.trim())) {
      return res.status(404).json({message: `Option search invalid!`});
    }
    if (salary_min || salary_max) {
      if (!salary_type) {
        return res.status(404).json({message: `salary_type is required!`});
      } else if (!lstSalaryType.includes(salary_type.toLowerCase())) {
        return res.status(404).json({message: `Salary type invalid!`});
      }
    }
    const jobs = await searchJobByOption(
      option,
      query,
      salary_min,
      salary_max,
      salary_type
    );
    return res.json({success: true, jobs});
  } catch (error) {
    logger.error(error);
    res.status(404).json({message: `Something went wrong!`});
  }
});

//Update job
router.put(
  '/update/:job_id',
  [requireAuthUser, validate(val.update)],
  async (req, res) => {
    try {
      const {user} = req;
      if (user.role != CONSTANTS.ROLES.ADMIN) {
        if (user.type != CONSTANTS.TYPE_USER.COMPANY) {
          return res.status(400).json({message: `You don't have permission!`});
        }
      }

      const {job_id} = req.params;
      const jobDto = req.body;
      const job = await findJobById(job_id);
      if (job) {
        let arrFiledNotUpdate = ['job_id', 'created_at', 'user_id'];
        let fieldsUpdate = Object.keys(jobDto);
        for (let field of fieldsUpdate) {
          if (!arrFiledNotUpdate.includes(field)) {
            job[field] = jobDto[field];
          }
        }
        job.positions = job.positions.toLowerCase();
        job.languages = job.languages.toLowerCase();
        const resultUpdate = await updateJob(job);
        if (resultUpdate.affectedRows > 0) {
          const jobAfterUpdate = await findJobById(job_id);
          return res.json({success: true, job: jobAfterUpdate});
        }
      }
    } catch (error) {
      logger.error(error);
      res.status(400).json({message: 'Something went wrong!'});
    }
  }
);

//get job detail
router.get(
  '/detail/:job_id',
  [requireAuthUser, validate(val.job_detail)],
  async (req, res) => {
    try {
      const {job_id} = req.params;
      const {user} = req;
      if (user.role != CONSTANTS.ROLES.ADMIN) {
        if (user.type != CONSTANTS.TYPE_USER.COMPANY) {
          return res.status(404).json({message: `You don't have permission!`});
        }
      }
      const job = await getDetail(user.user_id, job_id);
      if (job) {
        const users = await getUsersApply(job_id);
        job.applies = users;
        return res.json({success: true, job});
      }else {
        return res.status(404).json({message: 'Not found job!'})
      }
      console.log(job)
      
    } catch (error) {
      logger.error(error);
      res.status(400).json({message: 'Something went wrong!'});
    }
  }
);

module.exports = router;
