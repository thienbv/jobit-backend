'use strict';

const express = require('express');
const validate = require('express-validation');
const val = require('../validations/candidate');
const moment = require('moment');
const loggerCommon = require('../utils/logger');
const requireAuthUser = require('../middlewares/requireAuthUser');

const CONSTANTS = require('../constants/Globals');

const dotenv = require('dotenv');
dotenv.config();

const {
  createCandidate,
  updateCandidate,
  findAllCandidates,
  findCandidateByOption,
  isCandExisted
} = require('../models/candidateModel');

const logger = loggerCommon.getLogger('candidate-api');
const router = express.Router();

router.post(
  '/create',
  [requireAuthUser, validate(val.create)],
  async (req, res) => {
    const {user} = req;

    if (user.role != CONSTANTS.ROLES.ADMIN) {
      if (user.type != CONSTANTS.TYPE_USER.CANDIDATE) {
        return res.status(400).json({message: `You don't have permission!`});
      }
    }
    const candDto = req.body;
    candDto.position_apply = candDto.position_apply.toLowerCase();
    candDto.can_work_tool = candDto.can_work_tool.toLowerCase();
    candDto.user_id = user.user_id;
    const isExisted = await isCandExisted(user.user_id);
    if(isExisted) {
     return res.status(400).json({message: `You had created candidate!!`});
    }
    logger.info('================= Create CANDIDATE =================');
    try {
      logger.info(JSON.stringify(candDto));
      const resultCreate = await createCandidate(candDto);
      return res.send({
        success: true,
        cand_id: resultCreate.insertId,
      });
    } catch (err) {
      logger.error(err);
      res.status(400).json({
        success: false,
        messages: `Create Candidate fail ${err}`,
      });
    }
  }
);

//Get All User
router.get('/getAll', async (req, res) => {
  try {
    const candidates = await findAllCandidates();
    res.json({success: true, candidates});
  } catch (e) {
    logger.error(e);
    res.status(404).json({message: 'Something went wrong!'});
  }
});

//Get Job By UserID
router.get('/getByUser', requireAuthUser, async (req, res) => {
  try {
    const {user} = req;
    const candidate = await findCandidateByOption(
      user.user_id,
      CONSTANTS.TYPE_FIND.USER_ID
    );
    res.json({success: true, candidate});
  } catch (error) {
    logger.error(error);
    res.status(400).json({message: 'Something went wrong!'});
  }
});

router.put(
  '/update/:cand_id',
  [requireAuthUser, validate(val.update)],
  async (req, res) => {
    try {
      const {cand_id} = req.params;
      const candDto = req.body;
      const candidate = await findCandidateByOption(
        cand_id,
        CONSTANTS.TYPE_FIND.CAND_ID
      );
      if (candidate) {
        let arrFiledNotUpdate = ['cand_id', 'created_at', 'user_id'];
        let fieldsUpdate = Object.keys(candDto);
        for (let field of fieldsUpdate) {
          if (!arrFiledNotUpdate.includes(field)) {
            candidate[field] = candDto[field];
          }
        }
        candidate.position_apply = candidate.position_apply.toLowerCase();
        candidate.can_work_tool = candidate.can_work_tool.toLowerCase();
        const resultUpdate = await updateCandidate(candidate);
        if (resultUpdate.affectedRows > 0) {
          const candAfterUpdate = await findCandidateByOption(cand_id, CONSTANTS.TYPE_FIND.CAND_ID);
          res.json({success: true, candidate: candAfterUpdate});
        }else {
          res.status(400).json({message: 'Something went wrong!'});
        }
      }else {
        res.status(404).json({message: 'Not found candidate!'});
      }
    } catch (error) {
      logger.error(error);
      res.status(400).json({message: 'Something went wrong!'});
    }
  }
);
module.exports = router;
