'use strict';
const express = require('express');
const jwt = require('jsonwebtoken');
const {
  getMerchantByUserNameOffline,
  getMerchantByEmail,
  updatePasswordByWalletAddress,
} = require('../models/merchantModel');
const {checkPassword} = require('../utils/hash_password');
const router = express.Router();
const validate = require('express-validation');
const valid = require('../validations/merchant');
const {hashPassword} = require('../utils/hash_password');
const {generatePassword} = require('../utils/common');
const merchantService = require('../services/merchant');
const dotenv = require('dotenv');
dotenv.config();
const SECRET_KEY = process.env.SECRET_KEY || '#$#@432ADAiOxadsASadasd^%&@';

const nodemailer = require('nodemailer');


//Merchant Login
router.post('/login', async (req, res) => {
  const {username, password} = req.body;
  if (!username || !password) {
    return res.status(422).send({
      error: 'Must provide UserName and Password!',
    });
  }
  let merchant = await getMerchantByUserNameOffline(username);
  if (!merchant) {
    return res.status(422).send({
      error: 'Invalid Password or UserName',
    });
  }
  try {
    const checkPass = await checkPassword(password, merchant.password);
    if (checkPass) {
      req.body.walletAddress = merchant.wallet_address;
      const merchantOnline = await merchantService.findMerchantByWalletAddress(req);
      merchant.type = merchantOnline.Type;
      merchant.status = merchantOnline.Status;
      merchant.token_amount = merchantOnline.TokenAmount;
      merchant.rate_cash = merchantOnline.RateCash;
      const expired = '1h'; //1hour
      const dataSign = {
        walletAddress: merchant.wallet_address,
      };
      const token = jwt.sign(dataSign, SECRET_KEY, {
        expiresIn: expired,
      });
      delete merchant.password;
      return res.send({
        token,
        merchant,
      });
    } else {
      return res.status(422).send({
        error: 'Invalid Password or UserName',
      });
    }
  } catch (e) {
    console.log(e);
    return res.status(422).send({
      error: 'Invalid Password or UserName',
    });
  }
});

//Check valid token
router.post('/token_valid', (req, res) => {
  const {authorization} = req.headers;
  if (!authorization) {
    return res.status(401).send({
      valid: false,
    });
  }

  const token = authorization.replace('Bearer ', '');
  jwt.verify(token, SECRET_KEY, async (err, payload) => {
    if (err) {
      return res.status(401).send({
        valid: false,
      });
    }
    res.send({
      valid: true,
    });
  });
});

//forgot password
router.post('/forgot', validate(valid.forgot), async (req, res) => {
  try {
    const {email} = req.body;
    //Get merchant by email
    const merchant = await getMerchantByEmail(email);
    if (!merchant) {
      return res.status(404).send({messages: ['Your email not registered']});
    }
    const password = await generatePassword(7);
    const hashPass = hashPassword(password);
    const updateMerchant = await updatePasswordByWalletAddress(
      hashPass,
      merchant.wallet_address
    );

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: 'ilm.incorps@gmail.com', // generated ethereal user
        pass: 'Passw01#', // generated ethereal password
      },
    });
    let contentEmail = `<h2>Your Password Has Been Changed!</h2>
    <p>This email confirm that your password has been changed</p>
    <p>To logon to the site, use the following credentials</p>
    <p><b>Username: ${merchant.user_name}</b></p>
    <p><b>Password: ${password}</b></p>
    <p>If you have any questions or encounter any problems logging in, please contact a site administrator.</p>
    `;
    let subject = `Your password has been changed!`;
    let info = await transporter.sendMail({
      from: 'Admin ILM <ilm.incorps@gmail.com>', // sender address
      to: email, // list of receivers
      subject: subject, // Subject line
      html: contentEmail, // html body
    });
    res.send({
      success: true,
    });
    console.log('Message sent: %s', info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview only available when sending through an Ethereal account
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  } catch (e) {
    console.log(e);
    res.status(404).send({
      success: false,
      messages: [e.message],
    });
  }
});

module.exports = router;
