'use strict';

const express = require('express');
const validate = require('express-validation');
const val = require('../validations/cv.js');
const moment = require('moment');
const loggerCommon = require('../utils/logger');
const requireAuthUser = require('../middlewares/requireAuthUser');

const CONSTANTS = require('../constants/Globals');
//cv methods
const {
  createCV,
  findCVById,
  findCVByOption,
  updateCV,
  findCVByIdAndUserId,
  searchCVByOption
} = require('../models/cvModel');
const dotenv = require('dotenv');
dotenv.config();

const logger = loggerCommon.getLogger('cv-api');
const router = express.Router();

const createCVs = async (cvDtos, user_id) => {
  const insertIds = [];
  for (let cvDto of cvDtos) {
    cvDto.work_position = cvDto.work_position.toLowerCase();
    cvDto.work_times = cvDto.work_times.toLowerCase();
    cvDto.programs = cvDto.programs.toLowerCase();
    cvDto.members = cvDto.members;
    cvDto.demo_link = cvDto.demo_link;
    cvDto.user_id = user_id;
    const resultCreate = await createCV(cvDto);
    insertIds.push(resultCreate.insertId);
  }
  return insertIds;
};

const updateCVs = async (cvDtos, user_id) => {
  const insertIds = [];
  for (let cvDto of cvDtos) {
    const cv = await findCVByIdAndUserId(cvDto.cv_id, user_id);
    if (cv) {
      cvDto.work_position = cvDto.work_position.toLowerCase();
      cvDto.work_times = cvDto.work_times.toLowerCase();
      cvDto.programs = cvDto.programs.toLowerCase();

      const keys = Object.keys(cvDto);
      for (let k of keys) {
        cv[k] = cvDto[k];
      }
      const updated = await updateCV(cv);
      if (updated.affectedRows > 0) insertIds.push(cv.cv_id);
    }
  }
  return insertIds;
};

router.post(
  '/create',
  [requireAuthUser, validate(val.create)],
  async (req, res) => {
    const {user} = req;

    if (
      user.role != CONSTANTS.ROLES.ADMIN &&
      user.type != CONSTANTS.TYPE_USER.CANDIDATE
    ) {
      return res.status(400).json({message: `You don't have permission!`});
    }
    const cvDtos = req.body;

    logger.info('================= Create CV =================');
    try {
      logger.info(JSON.stringify(cvDtos.cvs));
      const insertIds = await createCVs(cvDtos.cvs, user.user_id);
      return res.json({success: true, cv_ids: insertIds});
    } catch (err) {
      logger.error(err);
      res.status(400).json({
        success: false,
        messages: `Create CV fail ${err}`,
      });
    }
  }
);

//Update cv
router.post(
  '/update',
  [requireAuthUser, validate(val.update)],
  async (req, res) => {
    const {user} = req;

    if (
      user.role != CONSTANTS.ROLES.ADMIN &&
      user.type != CONSTANTS.TYPE_USER.CANDIDATE
    ) {
      return res.status(400).json({message: `You don't have permission!`});
    }
    const cvDtos = req.body;

    logger.info('================= Update CV =================');
    try {
      logger.info(JSON.stringify(cvDtos.cvs));
      const insertIds = await updateCVs(cvDtos.cvs, user.user_id);
      return res.json({success: true, cv_ids: insertIds});
    } catch (err) {
      logger.error(err);
      res.status(400).json({
        success: false,
        messages: `Update CV fail ${err}`,
      });
    }
  }
);

//Get CV By User For company
router.get('/user/:user_id', requireAuthUser, async (req, res) => {
  try {
    const {user} = req;
    if (user.role != CONSTANTS.ROLES.ADMIN) {
      if (user.type != CONSTANTS.TYPE_USER.COMPANY) {
        return res.status(404).json({message: `You don't have permission!`});
      }
    }
    const {user_id} = req.params;
    const cvs = await findCVByOption(user_id, CONSTANTS.TYPE_FIND.USER_ID);
    return res.json({success: true, cvs});
  } catch (error) {
    logger.error(error);
    return res.status(404).json({message: 'Something went wrong!'});
  }
});

//Get list cv of user
router.get('/user', requireAuthUser, async (req, res) => {
  try {
    const {user} = req;
    if (user.role != CONSTANTS.ROLES.ADMIN) {
      if (user.type != CONSTANTS.TYPE_USER.CANDIDATE) {
        return res.status(404).json({message: `You don't have permission!`});
      }
    }
    const cvs = await findCVByOption(user.user_id, CONSTANTS.TYPE_FIND.USER_ID);
    return res.json({success: true, cvs});
  } catch (error) {
    logger.error(error);
    return res.status(404).json({message: 'Something went wrong!'});
  }
});

router.get('/search', requireAuthUser, async (req, res) => {
  try {
    console.log(req.query)
    const {user} = req;
    if(user.role != CONSTANTS.ROLES.ADMIN) {
      if(user.type != CONSTANTS.TYPE_USER.COMPANY) {
        return res.status(404).json({message: `You don't have permission!`});
      }
    }
    
    const {option, query} = req.query;
    const lstOption = [
      CONSTANTS.OPTION_SEARCH_CV.EMAIL,
      CONSTANTS.OPTION_SEARCH_CV.USER_NAME,
      CONSTANTS.OPTION_SEARCH_CV.PHONE,
      CONSTANTS.OPTION_SEARCH_CV.PROGRAM,
    ];
    if(!lstOption.includes(option)) {
      return res.status(404).json({message: `Option search invalid!`});
    }

    const cvs = await searchCVByOption(option, query);
    return res.json({success: true, cvs});

  } catch (err) {
    logger.error(err);
    return res.status(404).json({message: 'Something went wrong!'});
  }
});

module.exports = router;
