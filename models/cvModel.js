const {execute} = require('../db/jobit_db');
const CONSTANTS = require('../constants/Globals');

const createCV = async cvDto => {
  const sql = `
  INSERT INTO cvs (cv_id, user_id, project_name, project_intro, work_position, members, work_times, programs, demo_link, created_at, updated_at) 
  VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, current_timestamp(), current_timestamp());  
    `;

  let {
    user_id,
    project_name,
    project_intro,
    work_position,
    members,
    work_times,
    programs,
    demo_link,
  } = cvDto;

  const cv = await execute(sql, [
    user_id,
    project_name,
    project_intro,
    work_position,
    members,
    work_times,
    programs,
    demo_link,
  ]);
  return cv;
};

const updateCV = async cvDto => {
  const sql = `
    UPDATE cvs SET project_name = ?, project_intro = ?, work_position = ?, members = ?, work_times = ?, programs = ?, demo_link = ?, updated_at = current_timestamp() 
    WHERE cv_id = ?
  `;
  const {
    project_name,
    project_intro,
    work_position,
    members,
    work_times,
    programs,
    demo_link,
    cv_id,
  } = cvDto;
  const result = await execute(sql, [
    project_name,
    project_intro,
    work_position,
    members,
    work_times,
    programs,
    demo_link,
    cv_id,
  ]);
  return result;
};

const findCVByOption = async (value, option) => {
  let sql = '';
  switch (option) {
    case CONSTANTS.TYPE_FIND.EMAIL:
      sql = `SELECT cvs.*, user_name, email, contact, address, phone FROM cvs AS CV, users AS U WHERE U.user_id = CV.user_id AND U.email = ?`;
      break;
    case CONSTANTS.TYPE_FIND.USER_ID:
      sql = `SELECT cvs.* , user_name, email, contact, address, phone FROM cvs, users WHERE cvs.user_id = users.user_id AND user_id = ?`;
      break;
  }
  const user = await execute(sql, [value]);
  return user;
};

const searchCVByOption = async (option, query) => {
  query = query.trim();
  let sql = ` SElECT cvs.*, user_name, email, phone, contact, address, avatar FROM users, cvs 
  WHERE users.user_id = cvs.user_id AND `;
  switch (option) {
    case CONSTANTS.OPTION_SEARCH_CV.EMAIL:
      sql += ` email = ? `;
      break;
    case CONSTANTS.OPTION_SEARCH_CV.PHONE:
      sql += ` phone = ? `;
      break;
    case CONSTANTS.OPTION_SEARCH_CV.USER_NAME:
      sql += ` user_name LIKE ? `;
      query = `%${query}%`;
      break;
    case CONSTANTS.OPTION_SEARCH_CV.PROGRAM:
      sql += ` programs LIKE ? `;
      query = `%${query}%`;
      break;
  }
  const cvs = await execute(sql, [query]);
  return cvs;
};

const findCVById = async cvId => {
  const sql = `SELECT cvs.*, user_name, email, contact, address, phone FROM cvs, users WHERE cv_id = ? AND cvs.user_id = users.user_id`;
  const cv = await execute(sql, [cvId]);
  return cv[0];
};
const findCVByIdAndUserId = async (cvId, user_id) => {
  const sql = `SELECT * FROM cvs WHERE cv_id = ? AND user_id = ?`;
  const cv = await execute(sql, [cvId, user_id]);
  return cv[0];
};

module.exports = {
  createCV,
  updateCV,
  findCVByOption,
  findCVById,
  findCVByIdAndUserId,
  searchCVByOption,
};
