const {execute} = require('../db/jobit_db');
const CONSTANTS = require('../constants/Globals');

const createJob = async jobDto => {
  const sql = `
  INSERT INTO jobs (job_id, user_id, title, excerpt, information, salary_min, salary_max, salary_type, location, positions, 
    languages, experience, age_min, age_max, gender, start_find, expired_find, benefit, created_at, updated_at)
   VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, current_timestamp(), current_timestamp());
    `;

  let {
    user_id,
    title,
    excerpt,
    information,
    salary_min,
    salary_max,
    salary_type,
    location,
    positions,
    languages,
    experience,
    age_min,
    age_max,
    gender,
    start_find,
    expired_find,
    benefit,
  } = jobDto;

  const job = await execute(sql, [
    user_id,
    title,
    excerpt,
    information,
    salary_min,
    salary_max,
    salary_type,
    location,
    positions,
    languages,
    experience,
    age_min,
    age_max,
    gender,
    start_find,
    expired_find,
    benefit,
  ]);
  return job;
};
//update job
const updateJob = async jobDto => {
  console.log(jobDto);
  const sql = `
  UPDATE jobs SET title = ?, excerpt = ?, information = ?, salary_min = ?, salary_max = ?, salary_type = ?,
    location = ?,  positions = ?, languages = ?, experience = ?, age_min = ?, age_max = ?, gender = ?, start_find = ?, 
    expired_find = ?, benefit = ?, updated_at = current_timestamp()
  WHERE job_id = ?
  `;
  const {
    title,
    excerpt,
    information,
    salary_min,
    salary_max,
    salary_type,
    location,
    positions,
    languages,
    experience,
    age_min,
    age_max,
    gender,
    start_find,
    expired_find,
    benefit,
    job_id,
  } = jobDto;
  const job = await execute(sql, [
    title,
    excerpt,
    information,
    salary_min,
    salary_max,
    salary_type,
    location,
    positions,
    languages,
    experience,
    age_min,
    age_max,
    gender,
    start_find,
    expired_find,
    benefit,
    job_id,
  ]);
  return job;
};
const commonSqlFindJob = `job_id, title, excerpt, salary_min, salary_max, salary_type, location, positions, user_name, address, avatar, email, phone, start_find, expired_find `;
const findJobByUserId = async userId => {
  const sql = `SELECT ${commonSqlFindJob} FROM jobs AS JB, users AS U WHERE U.user_id = JB.user_id AND  JB.user_id = ?`;
  const job = await execute(sql, [userId]);
  return job;
};
const findAll = async () => {
  const sql = `SELECT ${commonSqlFindJob} FROM jobs AS JB, users AS U WHERE U.user_id = JB.user_id `;
  const jobs = await execute(sql);
  return jobs;
};
const findJobById = async job_id => {
  const sql = `SELECT JB.*, user_name, address, avatar, email, phone FROM jobs AS JB, users AS U WHERE U.user_id = JB.user_id AND job_id = ?`;
  const job = await execute(sql, [job_id]);
  return job[0];
};

//Get job detail of user
const getDetail = async (user_id, job_id) => {
  const sql = `
  SELECT JB.*, user_name, address, avatar, email, phone FROM jobs AS JB, users AS U WHERE U.user_id = JB.user_id AND JB.job_id = ? AND JB.user_id = ?
  `
  const job = await execute(sql, [job_id, user_id]);
  return job[0];
}

//Get list user apply job
const getUsersApply = async(job_id) => {
  const sql = `
    SELECT U.user_id, U.avatar, U.user_name, U.email, U.phone, U.address, U.contact FROM users as U, applies AS A WHERE U.user_id = A.user_id AND job_id = ?
  `
  const users = await execute(sql, [job_id]);
  return users;
}
const searchJobByOption = async (option, query, salary_min, salary_max, salary_type) => {
  let sql = ` SELECT ${commonSqlFindJob} FROM jobs AS JB, users AS U WHERE U.user_id = JB.user_id AND `;
  let params = [];
  switch (option) {
    case CONSTANTS.OPTION_SEARCH_JOB.COMPANY:
      sql += ` user_name LIKE ? `;
      break;
    case CONSTANTS.OPTION_SEARCH_JOB.POSITION:
      sql += ` positions LIKE ? `;
      break;
    case CONSTANTS.OPTION_SEARCH_JOB.LOCATION:
      sql += ` location LIKE ? `;
      break;
  }
  query = '%' + query.trim() + '%';
  params.push(query);

  if(salary_min) {
    sql += ` AND salary_min >= ? `
    params.push(salary_min);
  }

  if(salary_max) {
    sql += ` AND salary_max <= ? `
    params.push(salary_max);
  }

  if(salary_type) {
    sql += ` AND salary_type = ? `
    params.push(salary_type);
  }
  const jobs = await execute(sql, params);
  return jobs;
};
module.exports = {
  createJob,
  findJobByUserId,
  findAll,
  findJobById,
  updateJob,
  searchJobByOption,
  getDetail,
  getUsersApply
};
