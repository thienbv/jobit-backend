const {execute} = require('../db/jobit_db');
const CONSTANTS = require('../constants/Globals');

const createCandidate = async candDto => {
  const sql = `
    INSERT INTO candidates (cand_id, experience, graduate, salary_min, salary_max, position_apply, can_work_tool, work_location, user_id, created_at, updated_at)
     VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, current_timestamp(), current_timestamp());
    `;

  let {
    experience,
    graduate,
    salary_min,
    salary_max,
    position_apply,
    can_work_tool,
    work_location,
    user_id,
  } = candDto;
  const candidate = await execute(sql, [
    experience,
    graduate,
    salary_min,
    salary_max,
    position_apply,
    can_work_tool,
    work_location,
    user_id,
  ]);
  return candidate;
};

const updateCandidate = async candDto => {
  console.log(candDto)
  const sql = `
    UPDATE candidates SET experience = ?, graduate = ?, salary_min = ? , salary_max = ?, position_apply = ?, can_work_tool =? , work_location = ?
    WHERE cand_id = ?
  `;
  const {
    experience,
    graduate,
    salary_min,
    salary_max,
    position_apply,
    can_work_tool,
    wok_location,
    cand_id,
  } = candDto;

  const cand = await execute(sql, [
    experience,
    graduate,
    salary_min,
    salary_max,
    position_apply,
    can_work_tool,
    wok_location,
    cand_id,
  ]);
  return cand;
};
const findCandidateByOption = async (value, option) => {
  let optionFind = '';
  switch (option) {
    case CONSTANTS.TYPE_FIND.USER_ID:
      optionFind = ' user_id = ? ';
      break;
    case CONSTANTS.TYPE_FIND.CAND_ID:
      optionFind = ' cand_id = ? ';
      break;
  }
  const sql = `SELECT * FROM candidates WHERE ${optionFind}`;
  const candidate = await execute(sql, [value]);
  return candidate[0];
};

const findAllCandidates = async () => {
  const sql = `SELECT * FROM candidates`;
  const candidates = await execute(sql);
  return candidates;
};
const isCandExisted = async (user_id) => {
  const sql = `SELECT * FROM candidates WHERE user_id = ?`;
  const candidate = await execute(sql, [user_id]);
  return candidate.length > 0;
}
module.exports = {
  createCandidate,
  findCandidateByOption,
  findAllCandidates,
  updateCandidate,
  isCandExisted
};
