const {execute} = require('../db/jobit_db');
const CONSTANTS = require('../constants/Globals');

const createVote = async voteDto => {
  const sql = `
    INSERT INTO votes (vote_id, user_id, user_id_voted, comment, vote, created_at, updated_at)
     VALUES (NULL, ?, ?, ?, ?, current_timestamp(), current_timestamp());
    `;

  let {
    user_id,
    user_id_voted,
    comment,
    vote
  } = voteDto;
  const report = await execute(sql, [
    user_id,
    user_id_voted,
    comment,
    vote
  ]);
  return report;
};

const updateVote = async voteDto => {
  const sql = `
    UPDATE votes SET comment = ?, vote = ?
    WHERE vote_id = ?
  `;
  const {
    comment,
    vote,
    vote_id,
  } = voteDto;

  const result = await execute(sql, [
    comment,
    vote,
    vote_id,
  ]);
  return result;
};
const findVoteById = async (vote_id) => {
  const sql = `SELECT * FROM votes WHERE vote_id = ?`;
  const vote = await execute(sql, [vote_id]);
  return vote[0];
};

const findVoteByUserId = async (user_id, type) => {
  let optionFind = "";
  switch(type) {
    case CONSTANTS.FIND_VOTE.VOTER:
      optionFind = `user_id = ?`;  
    break;
    case CONSTANTS.FIND_VOTE.VOTED:
        optionFind = `user_id_voted = ?`;
      break;

  }
  const sql = `SELECT * FROM votes WHERE ${optionFind}`;
  const votes = await execute(sql, [user_id]);
  return votes;
};


module.exports = {
  createVote,
  updateVote,
  findVoteById, 
  findVoteByUserId
};
