const {execute} = require('../db/jobit_db');
const CONSTANTS = require('../constants/Globals');

const applyJob = async (user_id, job_id) => {
  const sql = `
  INSERT INTO applies (id, user_id, job_id, created_at, updated_at) 
  VALUES (NULL, ?, ?, current_timestamp(), current_timestamp());  
    `;
  const result = await execute(sql, [user_id, job_id]);
  return result;
};

const isApplyJob = async(user_id, job_id) => {
  const sql = `
    SELECT id FROM applies WHERE user_id = ? AND job_id = ?
  `
  const result = await execute(sql, [user_id, job_id]);
  return result.length > 0;

}
module.exports = {
  applyJob,
  isApplyJob
};
