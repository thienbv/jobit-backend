const {execute} = require('../db/jobit_db');
const CONSTANTS = require('../constants/Globals');

const createReport = async reportDto => {
  const sql = `
    INSERT INTO reports (report_id, user_id, user_id_report, comment, created_at, updated_at)
     VALUES (NULL, ?, ?, ?, current_timestamp(), current_timestamp());
    `;

  let {
    user_id,
    user_id_reported,
    comment,
  } = reportDto;
  const report = await execute(sql, [
    user_id,
    user_id_reported,
    comment,
  ]);
  return report;
};

const updateReport = async reportDto => {
  const sql = `
    UPDATE reports SET comment = ?
    WHERE report_id = ?
  `;
  const {
    comment,
    report_id,
  } = reportDto;

  const report = await execute(sql, [
    comment,
    report_id,
  ]);
  return report;
};
const findReportById = async (report_id) => {
  const sql = `SELECT * FROM reports WHERE report_id = ?`;
  const report = await execute(sql, [report_id]);
  return report[0];
};

const findReportByUserId = async (user_id, type) => {
  let optionFind = "";
  switch(type) {
    case CONSTANTS.FIND_REPORT.REPORTER:
      optionFind = `user_id = ?`;  
    break;
    case CONSTANTS.FIND_REPORT.REPORTED:
        optionFind = `user_id_reported = ?`;
      break;

  }
  const sql = `SELECT * FROM reports WHERE ${optionFind}`;
  const reports = await execute(sql, [user_id]);
  return reports;
};


module.exports = {
  createReport,
  updateReport,
  findReportById, 
  findReportByUserId
};
