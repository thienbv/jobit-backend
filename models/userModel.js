const {execute} = require('../db/jobit_db');
const {hashPassword, checkPassword} = require('../utils/hash_password');
const CONSTANTS = require('../constants/Globals');
const createUser = async userDto => {
  const sql = `
        INSERT INTO users (user_id, user_name, password, birth_day, email, phone, address, contact, type, avatar, gender, role, created_at, updated_at) 
        VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, current_timestamp(), current_timestamp());
    `;

  let {
    userName,
    password,
    birthDay,
    email,
    phone,
    address,
    contact,
    type,
    avatar,
    gender,
    role,
  } = userDto;
  password = hashPassword(password);

  const user = await execute(sql, [
    userName,
    password,
    birthDay,
    email,
    phone,
    address,
    contact,
    type,
    avatar,
    gender,
    role
  ]);
  return user;
};

const isExistUser = async(email) => {
    const sql = `SELECT 1 FROM users WHERE email = ?`
    const users = await execute(sql, [email]);
    return users.length > 0;
}
const findUserByOption = async (value, option) => {
  let optionFind = '';
  switch (option) {
    case CONSTANTS.TYPE_FIND.EMAIL:
      optionFind = ' email = ? ';
      break;
    case CONSTANTS.TYPE_FIND.USER_ID:
      optionFind = ' user_id = ? ';
      break;
  }
  const sql = `SELECT * FROM users WHERE ${optionFind}`;
  const user = await execute(sql, [value]);
  return user[0];
};
const updateUser = async (user) => {
  const {user_name, avatar, contact, phone, birth_day, address, gender, user_id} = user;
  const sql =  `UPDATE users SET user_name = ?, avatar = ?, contact = ?, phone = ?, birth_day = ?, address = ?, gender = ?, updated_at = current_timestamp() WHERE user_id = ?`;
  const params = [user_name, avatar, contact, phone, birth_day, address, gender, user_id];
  const result = await execute(sql, params);
  return result;
}

const findAllUser = async () => {
    const sql = `SELECT * FROM users`
    const users = await execute(sql);
    return users;
}

module.exports = {
  createUser,
  updateUser,
  findUserByOption,
  findAllUser,
  isExistUser
};
