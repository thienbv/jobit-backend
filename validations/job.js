'use strict';

const Joi = require('joi');

module.exports = {
  create: {
    body: {
      title: Joi.string().required(),
      excerpt: Joi.string().required(),
      information: Joi.string().required(),
      salary_min: Joi.number().required(),
      salary_max: Joi.number().required(),
      location: Joi.string().required(),
      positions: Joi.string().required(),
      start_find: Joi.date().required(),
      expired_find: Joi.date().required(),
      benefit: Joi.string().required(),
    },
  },
  update: {
    body: {
      title: Joi.string().required(),
      excerpt: Joi.string().required(),
      information: Joi.string().required(),
      salary_min: Joi.number().required(),
      salary_max: Joi.number().required(),
      location: Joi.string().required(),
      positions: Joi.string().required(),
      start_find: Joi.date().required(),
      expired_find: Joi.date().required(),
      benefit: Joi.string().required(),
    },
    params: {
      job_id: Joi.number().required(),
    },
  },
  job_detail: {
    params: {
      job_id: Joi.number().required(),
    }
  }
};
