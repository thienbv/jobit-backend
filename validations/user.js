'use strict';

const Joi = require('joi');

module.exports = {
  create: {
    body: {
      userName: Joi.string().required(),
      type: Joi.number().required(),
      birthDay: Joi.string().required(),
      email: Joi.string()
        .email()
        .required(),
      phone: Joi.number().required(),
      password: Joi.string()
        .required()
        .max(27)
        .min(7),
      confirmPassword: Joi.string()
        .required()
        .max(27)
        .min(7)
        .valid(Joi.ref('password'))
        .options({
          language: {
            any: {
              allowOnly: '!!Passwords do not match',
            },
          },
        }),
    },
  },
  create_vote: {
    body: {
      user_id_voted: Joi.number().required(),
      comment: Joi.string().required(),
      vote: Joi.number()
        .min(1)
        .max(5)
        .required(),
    },
  },
  update_vote: {
    body: {
      comment: Joi.string().required(),
      vote: Joi.number()
        .min(1)
        .max(5)
        .required(),
    },
    params: {
      vote_id: Joi.number().required(),
    },
  },
  create_report: {
    body: {
      user_id_reported: Joi.number().required(),
      comment: Joi.string().required(),
    },
  },
  update_report: {
    body: {
      comment: Joi.string().required(),
    },
    params: {
      report_id: Joi.number().required(),
    },
  },
};
