'use strict';

const Joi = require('joi');

module.exports = {
  create: {
    body: {
      experience: Joi.number().required(),
      graduate: Joi.string().required(),
      salary_min: Joi.number().required(),
      salary_max: Joi.number().required(),
      position_apply: Joi.string().required(),
      can_work_tool: Joi.string().required(),
      work_location: Joi.string().required(),
    },
  },
  update: {
    body: {
      experience: Joi.number().required(),
      graduate: Joi.string().required(),
      salary_min: Joi.number().required(),
      salary_max: Joi.number().required(),
      position_apply: Joi.string().required(),
      can_work_tool: Joi.string().required(),
      work_location: Joi.string().required(),
    },
    params: {
      cand_id: Joi.number().required(),
    },
  },
};
