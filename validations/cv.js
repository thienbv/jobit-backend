'use strict';

const Joi = require('joi');

module.exports = {
  create: {
    body: {
      cvs: Joi.array().items({
        project_name: Joi.string().required(),
        project_intro: Joi.string().required(),
        work_position: Joi.string().required(),
        members: Joi.number().required(),
        work_times: Joi.string().required(),
        programs: Joi.string().required(),
        demo_link: Joi.string().required(),
      }),
    },
  },
  update: {
    body: {
      cvs: Joi.array().items({
        cv_id: Joi.number().required(),
        project_name: Joi.string().required(),
        project_intro: Joi.string().required(),
        work_position: Joi.string().required(),
        members: Joi.number().required(),
        work_times: Joi.string().required(),
        programs: Joi.string().required(),
        demo_link: Joi.string().required(),
      }),
    },
  },
};
