'use strict';

const Joi = require('joi');

module.exports = {
  create: {
    body: {
      job_id: Joi.number().required(),
    },
  },
};
