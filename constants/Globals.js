const TYPE_FIND = {
  EMAIL: 1,
  USER_ID: 0,
  CAND_ID: 2,
};

const ROLES = {
  ADMIN: 1,
  USER: 0,
};

const TYPE_USER = {
  COMPANY: 1,
  CANDIDATE: 0,
};

const TYPE_GENDER = {
  MALE: 1,
  FEMALE: 2,
  BOTH: 0,
};

const FIND_REPORT = {
  REPORTER: 0,
  REPORTED: 1,
};

const FIND_VOTE = {
  VOTER: 0,
  VOTED: 1,
};

const OPTION_SEARCH_CV = {
  EMAIL:'email',
  USER_NAME: 'username',
  PHONE: 'phone',
  PROGRAM: 'program',
}
const OPTION_SEARCH_JOB = {
  COMPANY:'company',
  LOCATION: 'location',
  POSITION: 'position',
}
const SALARY_TYPE = {
  VND: 'vnd',
  USD: 'usd'
}

module.exports = {
  TYPE_FIND,
  TYPE_GENDER,
  TYPE_USER,
  ROLES,
  FIND_VOTE,
  FIND_REPORT,
  OPTION_SEARCH_CV,
  OPTION_SEARCH_JOB,
  SALARY_TYPE
};
