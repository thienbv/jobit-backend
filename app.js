'use strict';

const express = require('express');
// const fileUpload = require('express-fileupload');
const {json, urlencoded} = require('body-parser');
const http = require('http');
const cors = require('cors');
const loggerCommon = require('./utils/logger.js');
const logger = loggerCommon.getLogger('jobit-app');
const app = express();
const host = process.env.HOST || 'localhost';
const port = process.env.PORT || 1337;
const userControl = require('./routes/user');
const jobControl = require('./routes/job');
const candidateControl = require('./routes/candidate');
const cvControl = require('./routes/cv');


app.use(express.static('uploads'));
// app.use(fileUpload());
app.options('*', cors());
app.use(cors());
app.use(json());
app.use(
  urlencoded({
    extended: true,
  })
);

app.use('/user', userControl);
app.use('/job', jobControl);
app.use('/candidate', candidateControl);
app.use('/cv', cvControl);
app.use('/', (req, res) => {
  res.send('IT Work')
})
app.use((err, req, res, next) => {

  if (err) {
    if (err.name == 'ValidationError' || err.name == 'Error') {
      logger.error('ValidationError: ', err);
      const {errors} = err;
      console.log(err)
      const messages = errors.map(err => err.messages);
      return res.status(err.status).json({success: false, messages: messages});
    }
    if (err.name == 'MulterError') {
      logger.error('MulterError: ', err);
      return res.status(400).json({success: false, messages: err.message});
    }
    return res
      .status(400)
      .json({success: false, messages: 'Something went wrong!'});
  } else {
    next();
  }
});

const server = http.createServer(app).listen(port, () => {
  logger.info('****************** SERVER STARTED ************************');
  logger.info('***************  http://%s:%s  ******************', host, port);
});

server.timeout = 15000;