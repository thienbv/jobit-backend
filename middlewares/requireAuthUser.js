const jwt = require("jsonwebtoken");
const {findUserByOption} = require('../models/userModel')
const CONSTANTS = require('../constants/Globals');
module.exports = async (req, res, next) => {
  const SECRET_KEY = process.env.SECRET_KEY;
  const { authorization } = req.headers;
  if (!authorization) {
    return res.status(401).send({ messages: "You must be logged in!" });
  }

  const token = authorization.replace("Bearer ", "");
  jwt.verify(token, SECRET_KEY, async (err, payload) => {
    if (err) {
      return res.status(401).send({ messages: "You must be logged in!" });
    }
    const { user_id } = payload;
    const user = await findUserByOption(user_id, CONSTANTS.TYPE_FIND.USER_ID);
    req.user = user;
    next();
  });
};
