const merchantService = require('../services/merchant');

module.exports = async (req, res, next) => {
  const walletAddress = req.merchant.wallet_address;
  const operator = await merchantService.findMerchantByWalletAddress({body: {walletAddress}})
  if(!operator || operator.Type != 'Operator') {
      return res.status(404).send({ messages: "You don't have permission!" });
    }
  next();
};
