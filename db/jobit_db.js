
const util = require('util');
const mysql = require('mysql');
const loggerCommon = require('../utils/logger')
const logger = loggerCommon.getLogger('jobit-query');
const {dbConfig} = require('../config')
require('dotenv').config();
const configDb = {
  connectionLimit: dbConfig.connectionLimit,
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
};

const dbJobit = () => {
  const connection = mysql.createConnection(configDb);
  return {
    query(sql, args) {
      return util.promisify(connection.query).call(connection, sql, args);
    },
    close() {
      return util.promisify(connection.end).call(connection);
    },
  };
};
const execute = async (query, params) => {
  const connection = dbJobit();
  try {
    logger.info(`\nSQL: ${query} \nPARAMS: ${params}`)
    const results = await connection.query(query, params)
    return results;
  }catch(e) {
    logger.error(`ERROR SQL: ${query} with params: ${params}: `, e.message || e.stack);
    throw new Error(e);
  }finally {
    await connection.close();
  }
}
module.exports = {execute};
