const cloudinary = require('cloudinary').v2;
const dotenv = require('dotenv');
dotenv.config();
const {CLOUDINARY_NAME, CLOUD_API_KEY, CLOUD_API_SECRET} = process.env;
cloudinary.config({
  cloud_name: CLOUDINARY_NAME,
  api_key: CLOUD_API_KEY,
  api_secret: CLOUD_API_SECRET,
});
module.exports = {cloudinary};
